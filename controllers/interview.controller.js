const Web3API = require('web3');

exports.getETHBalanceOfWalletAddress = async (req, res) => {
    try {
        // I reused the Infura KEY found in `API/tansferAPIs.js`
        const web3 = new Web3API(new Web3API.providers.HttpProvider('https://mainnet.infura.io/v3/a1361e1c0fda468bab200985724259bd'));
        const address = req.body.address;
        const balance = await web3.eth.getBalance(address);
        // Convert balance from wei to Ether as float,
        // as no specification was provided for balance formatting
        const etherBalance = web3.utils.fromWei(balance, 'ether');
        res.json({ balance: etherBalance });
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }
}

exports.getUSDTBalanceOfWalletAddress = async (req, res) => {
    try {
        // I reused the Infura KEY found in `API/tansferAPIs.js`
        const web3 = new Web3API(new Web3API.providers.HttpProvider('https://mainnet.infura.io/v3/a1361e1c0fda468bab200985724259bd'));
        const address = req.body.address;
        // USDT token contract address and ABI
        const usdtContractAddress = '0xdAC17F958D2ee523a2206206994597C13D831ec7';
        const usdtContractABI = [
          {
            constant: true,
            inputs: [{ name: '_owner', type: 'address' }],
            name: 'balanceOf',
            outputs: [{ name: 'balance', type: 'uint256' }],
            type: 'function',
          },
        ];
        const usdtContract = new web3.eth.Contract(usdtContractABI, usdtContractAddress);
        const balance = await usdtContract.methods.balanceOf(address).call();
        // USDT has 6 decimals, so divide the balance by 10^6
        const usdtBalance = web3.utils.fromWei(balance, 'mwei');
        res.json({ balance: usdtBalance });
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }
}
